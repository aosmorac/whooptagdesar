<?php

/**
 * Clase para encapsular y abstraer el acceso a datos del usuario autenticado en el sistema.
 * @author Marino Perez
 */
class App_User {

    private static $_loggedUser = null;

    public function __construct() {
        throw new Exception("La clase App_User no debe ser instanciada. Utilice sus metodos de manera estatica.");
    }

    private static function init() {
        if (null === self::$_loggedUser) {
            self::$_loggedUser = new Zend_Session_Namespace("loggedUser");
        }
    }

    /**
     * Indica si existe un usuario autenticado en el sistema
     * @return true|false
     */
    public static function isLogged() {
        return Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * Obtiene el id del usuario autenticado en el sistema
     * @return integer o null si no hay un usuario autenticado
     */
    public static function getUserId() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->id_user;
        }
        return 0;
    }
    
    /**
     * Obtiene el Facebook id del usuario autenticado en el sistema
     * @return integer o null si no hay un usuario autenticado
     */
    public static function getUserFacebookId() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->Facebook_id;
        }
        return 0;
    }

    /**
     * Obtiene el Facebook nombreusuario del usuario autenticado en el sistema
     * @return string o null si no hay un usuario autenticado
     */
    public static function getUserFacebookName() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->Facebook_username;
        }
        return '';
    }

    /**
     * Obtiene los nombres del usuario autenticado en el sistema
     * @return string o null si no hay un usuario autenticado
     */
    public static function getName() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->use_first_name;
        }
        return '';
    }

    /**
     * Obtiene los apellidos del usuario autenticado en el sistema
     * @return string o null si no hay un usuario autenticado
     */
    public static function getLastName() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->use_last_name;
        }
        return '';
    }

    /**
     * Obtiene el nombre completo (nombres y apellidos) del usuario autenticado en el sistema
     * @return string o null si no hay un usuario autenticado
     */
    public static function getFullName() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->use_name;
        }else{
            return 'Un Amigo';
        }
    }

    

    /**
     * Obtiene un atributo del usuario 
     * autenticado
     * @return string o null si no hay un usuario autenticado
     */
    public static function getAttrib($attribute) {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return Zend_Auth::getInstance()->getIdentity()->$attribute;
        }
        return '';
    }

    

}

//fin de la clase
