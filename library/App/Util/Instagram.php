<?php

include_once '../library/Instagram/instagram.class.php';

class App_Util_Instagram {
	
    private static $client_id = '2016aa02fe05457486f0fd9cca91c751';	
    private static $client_secret = 'd68756a46e3a43479b693f67fb485feb';	
    private static $callback = 'http://localhost:8081';	
    
    public static function getTagMedia($tag, $pages = 1, $max_tag_id=0){
        
        // Initialize class for public requests
         $instagram = new Instagram(array(
            'apiKey'      => self::$client_id,
            'apiSecret'   => self::$client_secret,
            'apiCallback' => self::$callback 
         ));
         $next_max_id = $max_tag_id;
         $data = array();
        
         for ($i=1; $i<=$pages; $i++){
             $media = $instagram->getTagMedia($tag, $next_max_id);
             if (count($media->data)>0){
                if (isset($media->pagination->next_max_tag_id) && $media->pagination->next_max_tag_id != $next_max_id){
                    $next_max_id = $media->pagination->next_max_tag_id;
                }else{
                    $i=$pages+1;
                }
                foreach ($media->data as $m) {
                    $data[] = $m;
                }
             }else{
                 $i=$pages+1;
             }
         }
         
         return array('next_max_id'=>$next_max_id, 'data'=>$data);
    }
    
    public static function getTagMediaImages($tag, $pages = 1, $max_tag_id=0){
        
        // Initialize class for public requests
         $instagram = new Instagram(array(
            'apiKey'      => self::$client_id,
            'apiSecret'   => self::$client_secret,
            'apiCallback' => self::$callback 
         ));
         $next_max_id = $max_tag_id;
         $data = array();
         $index = 0;
        
         for ($i=1; $i<=$pages; $i++){
             $media = $instagram->getTagMedia($tag, $next_max_id);
             if (count($media->data)>0){
                if (isset($media->pagination->next_max_tag_id) && $media->pagination->next_max_tag_id != $next_max_id){
                    $next_max_id = $media->pagination->next_max_tag_id;
                }else{
                    $i=$pages+1;
                }
                foreach ($media->data as $m) {
                    $data[$index]['thumb'] = $m->images->thumbnail->url;
                    $data[$index]['short'] = $m->images->low_resolution->url;
                    $data[$index]['large'] = $m->images->standard_resolution->url;
                    $index++;
                }
             }else{
                 $i=$pages+1;
             }
         }
         
         return array('next_max_id'=>$next_max_id, 'data'=>$data);
    }
   	
}
