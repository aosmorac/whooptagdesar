<?php

include_once '../library/Twitter/twitteroauth.php';

class App_Util_Twitter {
    
    private static $consumer_key = 'wOfhwIm5ZlBEpEiWq6ul2A';
    private static $consumer_secret = 'MX6PhwTifqmWPMCz24I46PczkIlwAAR87hiVYx0gw';
    private static $access_token = '46398437-Uj267isROyPvqYzsTGsxvKdNgvNuGK7Yf1V1nYOuz';
    private static $access_token_secret = 'Cswu43OR3mCAgf0IMvJNWgM0iH85crlaZxLgpngvI';
    
    
    public static function getTweets($hash_tag, $count = 10, $max_id = 0, $since_id = 0) {
        $connection = new TwitterOAuth(self::$consumer_key, self::$consumer_secret, self::$access_token, self::$access_token_secret);
        $url = 'https://api.twitter.com/1.1/search/tweets.json?q='.urlencode($hash_tag).'&result_type=recent' ;
        $parameters = array();
        $parameters['q'] = $hash_tag;
        $parameters['result_type'] = 'recent';
        if ($count>0){
            $parameters['count'] = $count;
        }
        if ($max_id != 0){
            $parameters['max_id'] = $max_id;
        }
        if ($since_id != 0){
            $parameters['since_id'] = $since_id;
        }
        $result = $connection->get('search/tweets', $parameters);
        //Zend_Debug::dump($result); die;
        //return $result;
        return array('max_id'=>$result->search_metadata->max_id_str, 'since_id'=>$result->search_metadata->since_id_str, 'data'=>$result->statuses);
    }
	
	public static function getUsers($hash_tag){
		$connection = new TwitterOAuth(self::$consumer_key, self::$consumer_secret, self::$access_token, self::$access_token_secret);
        $url = 'https://api.twitter.com/1.1/users/search.json?q='.urlencode($hash_tag).'&count=4';
        $parameters = array();
        $parameters['q'] = $hash_tag;
		$parameters['count'] = 4;
        $result = $connection->get('users/search', $parameters);
        //Zend_Debug::dump($result); die;
        //return $result;
        return $result;
	}
	
	
	public static function getTweetsByUser($user_name, $count = 10, $max_id = 0, $since_id = 0) {
        $connection = new TwitterOAuth(self::$consumer_key, self::$consumer_secret, self::$access_token, self::$access_token_secret);
        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='.$user_name ;
        $parameters = array();
        $parameters['screen_name'] = $user_name;
        if ($count>0){
            $parameters['count'] = $count;
        }
        if ($max_id != 0){
            $parameters['max_id'] = $max_id;
        }
        if ($since_id != 0){
            $parameters['since_id'] = $since_id;
        }
        $result = $connection->get('statuses/user_timeline', $parameters);
        //Zend_Debug::dump($result); die;
        //return $result;
        $last = $result[count($result)-1];
        return array('max_id'=>$last->id, 'data'=>$result);
    }
   	
}
