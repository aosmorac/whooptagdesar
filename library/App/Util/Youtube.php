<?php

require_once '../library/google/Google_Client.php';
require_once '../library/google/contrib/Google_YouTubeService.php';

class App_Util_Youtube {
	
    private static $developer_key = 'AIzaSyDqPeF6eGogk2Z9w9wvH7lt750gSSQk1e8';

    
    public static function searchVideos($word_search, $maxResults = 10, $order = 'date', $page = ''){
        $client = new Google_Client();
        $client->setDeveloperKey(self::$developer_key);
        $youtube = new Google_YoutubeService($client);
		$htmlBody = '';
        try {
            $params = array(
                'q' => $word_search,
                'maxResults' => $maxResults,
                'order' => $order
            );
            if ($page != ''){
                $params['pageToken'] = $page;
            }
            $searchResponse = $youtube->search->listSearch('id,snippet', $params);
            if (!isset($searchResponse['nextPageToken'])){
                $searchResponse['nextPageToken'] = '';
            }
            return array('nextPageToken' => $searchResponse['nextPageToken'],
                         'totalResults' => $searchResponse['pageInfo']['totalResults'],
                         'data' => $searchResponse['items']);
        } catch (Google_ServiceException $e) {
            $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
        } catch (Google_Exception $e) {
            $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
        }
    }
	
	
}
