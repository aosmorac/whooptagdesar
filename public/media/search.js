/* JavaScript Document */



$(function() {
	/*var str = $.trim($('#search-field').val());
	 if (str.match(/[^0-9A-Za-z]/g)) {
       $('#search-field').val(str.replace(/[^0-9A-Za-z]/g, ' '));
     }*/
});


function startSearch(section){
	if ($.trim($('#search-field').val()) != ''){
		window.location.href = "/search/"+section+"/tag/"+($.trim($('#search-field').val())).replace(/\s/g,'+')+"/";
	}
}

function newSearch(){
	if ($.trim($('#search-field').val()) != ''){
		window.location.href = +($.trim($('#search-field').val())).replace(/\s/g,'+');
	}
}

var base_url = "http://desar.whooptag.com/";
//var base_url = "http://whooptag.com/";

var instagram_item;
var instagram_media;
var instagram_users = Object();

var twitter_item;
var twitter_media;

var youtube_item;
var youtube_media;

var screenview_item;

var all_item;
var map_item;

/*
 * Objeto para manejar funcionalidades de instgram
 * Directo al Api
 */
instagram = {
	
	/*
	* instagram.load();
	* Carga la información inicial
	*/
	load: function(callback){
		callback = callback || function(){};
		instagram_item = {
			  max_id: 0 
			, min_id: 0
			, api_id: '2016aa02fe05457486f0fd9cca91c751'	/* IMPORTANTE !! */
			, media: Object() 
			, page: {current: 0, last: 0}
			, isUser: 0
		};
		instagram_media = Object();
		callback();
	},
	
	/*
	* instagram.setUserMode();
	*/
	setUserMode: function(){
		instagram_item.isUser = 1;
	},
	
	/*
	* instagram.loadNewPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadNewPage: function(tag, callback){
		callback = callback || function(){};
		if (instagram_item.isUser == 1){
			if (instagram_item.max_id == 0) {
				var serv = "https://api.instagram.com/v1/users/"+tag+"/media/recent?client_id="+instagram_item.api_id;
			}else {
				var serv = "https://api.instagram.com/v1/users/"+tag+"/media/recent?max_id="+instagram_item.max_id+"&client_id="+instagram_item.api_id;
			}
		}else {
			if (instagram_item.max_id == 0) {
				var serv = "https://api.instagram.com/v1/tags/"+tag+"/media/recent?client_id="+instagram_item.api_id;
			}else {
				var serv = "https://api.instagram.com/v1/tags/"+tag+"/media/recent?max_id="+instagram_item.max_id+"&client_id="+instagram_item.api_id;
			}
		}
		$.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
		 	url: serv,
			success: function(data){
				if (data.meta.code == 200){
					if (instagram_item.isUser == 1){
						instagram_item.page.last ++;
						instagram_item.max_id = data.pagination.next_max_id;
						instagram_item.min_id = 0;
						instagram_item.media[instagram_item.page.last] = data.data;
						instagram_item.page.current = instagram_item.page.last;
						callback();
					}else{
						instagram_item.page.last ++;
						instagram_item.max_id = data.pagination.next_max_tag_id;
						instagram_item.min_id = 0;
						instagram_item.media[instagram_item.page.last] = data.data;
						instagram_item.page.current = instagram_item.page.last;
						callback();
					}
				}else {
					closeOverlay();
					loadError();
				}
			}
		});	
	},
	
	/*
	* instagram.loadPage(tag, pag);
	* Carga contenido en nueva pagina
	*/
	loadPage: function(tag, pag, callback){
		callback = callback || function(){};
		if (pag > 0){
			if ( typeof instagram_item.media[pag] !== 'undefined' ) {
				instagram_item.page.current = pag;
				callback();
			}else{
				instagram.loadNewPage(tag, function(){
					callback();
				});
			}
		}else {
			closeOverlay();
		}
	},
	
	/*
	* instagram.loadNextPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadNextPage: function(tag, callback){
		callback = callback || function(){};
		instagram.loadPage(tag, instagram_item.page.current+1, function(){
					callback();
				});
	},
	
	/*
	* instagram.loadPrevPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadPrevPage: function(tag, callback){
		callback = callback || function(){};
		instagram.loadPage(tag, instagram_item.page.current-1, function(){
					callback();
				});
	},
	
	/*
	* instagram.getPage();
	* Carga contenido en nueva pagina
	*/
	getPage: function(){
		return instagram_item.media[instagram_item.page.current];
	},
	
	/*
	* instagram.loadUsers(tag);
	* 
	*/
	loadUsers: function(tag, callback){
		callback = callback || function(){};
		var serv = "https://api.instagram.com/v1/users/search?q="+tag+"&client_id="+instagram_item.api_id;
		$.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
		 	url: serv,
			success: function(data){
				instagram_users = data.data;
				callback();
			}
		});	
	}, 
	
	/*
	* instagram.getUsers();
	* 
	*/
	getUsers: function(tag, callback){
		return instagram_users;
	},
	
	/*
	* instagram.saveMedia(id, element, callback);
	* 
	*/
	saveMedia: function(id, element, callback){
		callback = callback || function(){};
		var serv = base_url+"search/save-media/";
		$.ajax({
				 type: "POST",
				 url: serv,
			     async: false,
			     data: { network_id: id, element_data: element, network: 'instagram' },
			     success: function(data){ 
			     	 callback();
			     }
		    });	
	}
	
	
};

/*
 * Objeto para manejar funcionalidades de instgram
 * Directo al Api
 */
twitter = {
	/*
	* twitter.load();
	* Carga la información inicial
	*/
	load: function(callback){
		callback = callback || function(){};
		twitter_item = {
			  max_id: 0 
			, media: Object() 
			, page: {current: 0, last: 0}
			, isUser: 0
		};
		twitter_media = Object();
		callback();
	},
	
	/*
	* twitter.setUserMode();
	*/
	setUserMode: function(){
		twitter_item.isUser = 1;
	},
	
	/*
	* twitter.loadNewPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadNewPage: function(tag, callback){
		$.each( twitter_media, function( k, first ) {
			if (twitter_item.max_id == 0){ 
				twitter_item.max_id = k; 
			}else if(twitter_item.max_id > k){
				twitter_item.max_id = k; 
			}
		});
		callback = callback || function(){};
		if (twitter_item.isUser == 1){
			var serv = base_url+"search/get-twitters-by-user/uid/"+tag+"/max_id/"+twitter_item.max_id+"/a/1";
		}else {
			var serv = base_url+"search/get-twitters/tag/"+tag+"/max_id/"+twitter_item.max_id+"/a/1";
		}
		
		$.ajax({
            type: "GET",
            dataType: "json",
            cache: false,
		 	url: serv,
			success: function(data){ 
				twitter_item.page.last ++;
				twitter_item.max_id = data.max_id;
				twitter_item.media[twitter_item.page.last] = data.data;
				twitter_item.page.current = twitter_item.page.last;
				callback();
			}
		});	
	},
	
	/*
	* twitter.loadPage(tag, pag);
	* Carga contenido en nueva pagina
	*/
	loadPage: function(tag, pag, callback){
		callback = callback || function(){};
		if (pag > 0){
			if ( typeof twitter_item.media[pag] !== 'undefined' ) {
				twitter_item.page.current = pag;
				callback();
			}else{
				twitter.loadNewPage(tag, function(){
					callback();
				});
			}
		}else {
			closeOverlay();
		}
	},
	
	/*
	* twitter.loadNextPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadNextPage: function(tag, callback){
		callback = callback || function(){};
		twitter.loadPage(tag, twitter_item.page.current+1, function(){
					callback();
				});
	},
	
	/*
	* twitter.loadPrevPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadPrevPage: function(tag, callback){
		callback = callback || function(){};
		twitter.loadPage(tag, twitter_item.page.current-1, function(){
					callback();
				});
	},
	
	/*
	* twitter.getPage();
	* Carga contenido en nueva pagina
	*/
	getPage: function(){
		return twitter_item.media[twitter_item.page.current];
	},
	
	/*
	* twitter.saveMedia(id, element, callback);
	* 
	*/
	saveMedia: function(id, element, callback){
		callback = callback || function(){};
		var serv = base_url+"search/save-media/";
		$.ajax({
				 type: "POST",
				 url: serv,
			     async: false,
			     data: { network_id: id, element_data: element, network: 'twitter' },
			     success: function(data){ 
			     	 callback();
			     }
		    });	
	}
};




/*
 * Objeto para manejar funcionalidades de youtube
 * Directo al Api
 */
youtube = {
	/*
	* youtube.load();
	* Carga la información inicial
	*/
	load: function(callback){
		callback = callback || function(){};
		youtube_item = {
			  nextPageToken: ''
			, max_id: 0 
			, media: Object() 
			, page: {current: 0, last: 0}
		};
		youtube_media = Object();
		callback();
	},
	
	/*
	* youtube.loadNewPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadNewPage: function(tag, order, callback){
		callback = callback || function(){};
		var serv = base_url+"search/get-youtube/tag/"+tag+"/order/"+order+"/page/"+(youtube_item.nextPageToken)+"/a/1";
		$.ajax({
            type: "GET",
            dataType: "json",
            cache: false,
		 	url: serv,
			success: function(data){ 
				youtube_item.nextPageToken = data.nextPageToken;
				youtube_item.page.last ++;
				youtube_item.media[youtube_item.page.last] = data.data;
				youtube_item.page.current = youtube_item.page.last;
				callback();
			}
		});	
	},
	
	/*
	* youtube.loadPage(tag, pag);
	* Carga contenido en nueva pagina
	*/
	loadPage: function(tag, pag, order, callback){
		callback = callback || function(){};
		if (pag > 0){
			if ( typeof youtube_item.media[pag] !== 'undefined' ) {
				youtube_item.page.current = pag;
				callback();
			}else{
				youtube.loadNewPage(tag, order, function(){
					callback();
				});
			}
		}
	},
	
	/*
	* youtube.loadNextPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadNextPage: function(tag, order, callback){
		callback = callback || function(){};
		youtube.loadPage(tag, youtube_item.page.current+1, order, function(){
					callback();
				});
	},
	
	/*
	* youtube.loadPrevPage(tag);
	* Carga contenido en nueva pagina
	*/
	loadPrevPage: function(tag, order, callback){
		callback = callback || function(){};
		youtube.loadPage(tag, youtube_item.page.current-1, order, function(){
					callback();
				});
	},
	
	/*
	* youtube.getPage();
	* Carga contenido en nueva pagina
	*/
	getPage: function(){
		return youtube_item.media[youtube_item.page.current];
	},
	
	/*
	* twitter.saveMedia(id, element, callback);
	* 
	*/
	saveMedia: function(id, element, callback){
		callback = callback || function(){};
		var serv = base_url+"search/save-media/";
		$.ajax({
				 type: "POST",
				 url: serv,
			     async: false,
			     data: { network_id: id, element_data: element, network: 'youtube' },
			     success: function(data){ 
			     	 callback();
			     }
		    });	
	}
};


/*
 * Objeto para manejar funcionalidades de screenview
 * Directo al Api
 */
screenview = {
	/*
	* screenview.load();
	* Carga la información inicial
	*/
	load: function(callback){
		callback = callback || function(){};
		screenview_item = {
			  max_id_twitter: ''
			, max_id_instagram: ''
			, tag_instagram: ''
			, tag_twitter: ''
			, user_instagram: ''
			, user_twitter: ''
			, show: 'tag' /* tag user user-tag */ 
			, elements: Array() 
			, max_id_element: -1
		};
		callback();
	},
	
	/*
	* screenview.loadItems(callback);
	* Carga contenido 
	*/
	loadItems: function(callback){
		callback = callback || function(){};
		
			if (screenview_item.max_id_instagram == '') {
				var serv = "https://api.instagram.com/v1/tags/"+screenview_item.tag_instagram+"/media/recent?client_id="+instagram_item.api_id;
			}else {
				var serv = "https://api.instagram.com/v1/tags/"+screenview_item.tag_instagram+"/media/recent?min_id="+screenview_item.max_id_instagram+"&client_id="+instagram_item.api_id;
			}
	
		$.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
		 	url: serv,
			success: function(data){
				if (data.meta.code == 200){
					screenview_item.max_id_instagram = data.pagination.min_tag_id;
					var media = data.data;
					$.each(media, function( index, value ) {
						var e = {
							  source_id : value.id
							, network : 'instagram'
							, data : value
						};
						screenview_item.elements.push(e);
					});
					callback();
				}else {
					loadError();
				}
			}
		});	
	}
	
};



/*
 * Utilidades
 */
function truncate(cadena, size, pos){
	size = size || 20;
	pos = pos || '...';
	if (cadena.length <= size){ pos = ''; }
	return jQuery.trim(cadena).substring(0, size).trim(this) + pos;
}


/* unixtime fecha */
function formatInstagramDate(fecha){
			fecha = new Date(fecha*1000);
 			var year = fecha.getFullYear();
     		var month = fecha.getMonth()+1;
     		var day = fecha.getDate();
     		var hour = fecha.getHours();
     		var min = fecha.getMinutes();
     		var sec = fecha.getSeconds();
     		var fecha = year+'-'+( "0" + month).slice(-2)+'-'+( "0" + day).slice(-2)+' '+( "0" + hour).slice(-2)+':'+( "0" + min).slice(-2)+':'+( "0" + sec).slice(-2) ;
			/* document.write(fecha.toString()); */
			if (jQuery.trim(fecha)!=''){
				var today = new Date();
				var dateTimeSplit = jQuery.trim(fecha).split(' ');
	            var dateSplit = dateTimeSplit[0].split('-');
	            var currentDate = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0].substring(2, 4);
	            /* currentDate is 18/10/10 */
	            var currentTime = dateTimeSplit[1].substring(0, 5);
	            /* currentTime is 10:06 */
				if ((( "0" + dateSplit[2]).slice(-2)+'/'+( "0" + dateSplit[1]).slice(-2)+'/'+dateSplit[0]) == (("0" + today.getDate()).slice(-2)+'/'+("0" + (today.getMonth() + 1)).slice(-2)+'/'+today.getFullYear())){
					return currentTime;
				}else {
					return currentDate;
				}
			}else{
				return '';
			}
}

/* fecha */
function formatTwitterDate(fecha){
			fecha = new Date(fecha);
 			var year = fecha.getFullYear();
     		var month = fecha.getMonth()+1;
     		var day = fecha.getDate();
     		var hour = fecha.getHours();
     		var min = fecha.getMinutes();
     		var sec = fecha.getSeconds();
     		var fecha = year+'-'+( "0" + month).slice(-2)+'-'+( "0" + day).slice(-2)+' '+( "0" + hour).slice(-2)+':'+( "0" + min).slice(-2)+':'+( "0" + sec).slice(-2) ;
			/* document.write(fecha.toString()); */
			if (jQuery.trim(fecha)!=''){
				var today = new Date();
				var dateTimeSplit = jQuery.trim(fecha).split(' ');
	            var dateSplit = dateTimeSplit[0].split('-');
	            var currentDate = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0].substring(2, 4);
	            /* currentDate is 18/10/10 */
	            var currentTime = dateTimeSplit[1].substring(0, 5);
	            /* currentTime is 10:06 */
				if ((( "0" + dateSplit[2]).slice(-2)+'/'+( "0" + dateSplit[1]).slice(-2)+'/'+dateSplit[0]) == (("0" + today.getDate()).slice(-2)+'/'+("0" + (today.getMonth() + 1)).slice(-2)+'/'+today.getFullYear())){
					return currentTime;
				}else {
					return currentDate;
				}
			}else{
				return '';
			}
}


/* fecha */
function formatYoutubeDate(fecha){
			fecha = new Date(fecha);
 			var year = fecha.getFullYear();
     		var month = fecha.getMonth()+1;
     		var day = fecha.getDate();
     		var hour = fecha.getHours();
     		var min = fecha.getMinutes();
     		var sec = fecha.getSeconds();
     		var fecha = year+'-'+( "0" + month).slice(-2)+'-'+( "0" + day).slice(-2)+' '+( "0" + hour).slice(-2)+':'+( "0" + min).slice(-2)+':'+( "0" + sec).slice(-2) ;
			/* document.write(fecha.toString()); */
			if (jQuery.trim(fecha)!=''){
				var today = new Date();
				var dateTimeSplit = jQuery.trim(fecha).split(' ');
	            var dateSplit = dateTimeSplit[0].split('-');
	            var currentDate = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0].substring(2, 4);
	            /* currentDate is 18/10/10 */
	            var currentTime = dateTimeSplit[1].substring(0, 5);
	            /* currentTime is 10:06 */
				if ((( "0" + dateSplit[2]).slice(-2)+'/'+( "0" + dateSplit[1]).slice(-2)+'/'+dateSplit[0]) == (("0" + today.getDate()).slice(-2)+'/'+("0" + (today.getMonth() + 1)).slice(-2)+'/'+today.getFullYear())){
					return currentTime;
				}else {
					return currentDate;
				}
			}else{
				return '';
			}
}

function openSharedButtons(){
	$('#share-icons').show();
}
function openOverlay(){
	$('#overlay').show();
}
function openLoading(){
	$('#overlay_content').html('<div class="loading"><img src="/img/ajax-loader.gif" /></div>');
	$('#overlay').show();
}
function closeOverlay(callback){
	callback = callback || function(){};
	$('#overlay').hide();
	$('#share-icons').hide();
	$('#overlay_content').html("");
	callback();
}




/*
 * Facebook
 */
			function shareFacebook(urlImage){
				FB.ui({
				  method: 'feed',
				  redirect_uri: document.URL,
				  link: document.URL,
				  picture: urlImage,
				  name: name,
				  caption: 'Whooptag.com',
				  description: 'The Social Search Engine'
				}, function(response){});	
			}
/* Fin Facebook */


/* ScreenView */
	function openScreenView(){
		$('#screenview').show();
		$('body').css('overflow', 'hidden');
		screenview.load();
		screenview.loadItems(callback);
	}
	function closeScreenView(){
		$('#screenview').hide();
		$('body').css('overflow', 'auto');
	}
/* Fin ScreenView */
