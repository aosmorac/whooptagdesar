<?php

class User_Model_Emails {
	
	private $emailConfig;
	
    public function __construct() {
    	$emailConfig = Array(
			  'from' => 'info@squadrapp.com'
			, 'from_name' => 'Whooptag'
		);
    }
	
	public function confirmSignUp($user){
		$De = $emailConfig['from']; 
		$NombreDe = $emailConfig['from_name']; 
		$Para = $user['Email'];  
		$Asunto = 'Confirmación - Whooptag';  
		$CuerpoDeMensaje = '<p>'.$user['name'].', bienvenido a Whooptag</p><p>Da clic en el siguiente link o copia y pega la url en su explorador para confirmar su registro.<br/><a href="algo">Algo</a></p>'; 
		App_Util_Mail::mailSMTP($De, $Para, $Asunto, $CuerpoDeMensaje, $NombreDe);
	}    

    
}

