<?php

class User_Model_DbTable_User extends Zend_Db_Table_Abstract {

    protected $_name = "user";

    public function __construct() {
        $this->_setAdapter('SITE');
    }
	
	public function getItems($where='1=1', $limit=10){
		$rows = $this->fetchAll(
		    $this->select()
		        ->where($where)
		        ->limit($limit)
		    );
		return $rows->toArray();
	}
	
	public function getUser($where='1=1'){
		$row = $this->fetchRow(
		    $this->select()
		        ->where($where)
		        ->limit(1)
		    );
		return $row->toArray();
	}


}

