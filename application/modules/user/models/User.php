<?php

class User_Model_User {
	
    private $registroDataTable;
	
    public function __construct() {
        $this->registroDataTable = new User_Model_DbTable_User();
    }
	
	public function login($email, $password){
		$passwd = md5($password);
		$where = "use_email = '{$email}' AND use_password = '{$passwd}'";
		$user = $this->registroDataTable->getItems($where);
		if ( count($user) > 0 ){
			if ( $user['use_sta_id'] == 2 ){
				$auth = Zend_Auth::getInstance();
				$auth->getStorage()->write((object) $user);
				return 'success';
			}elseif ( $user['use_sta_id'] == 1 ){
				return 'unconfirmed';
			}
		}else {
			return 'empty';
		}
		Zend_Debug::dump($user); die;
	}
	
    public function saveElement($email, $name, $lastname, $password){
    		$data = Array('use_email'=>$email, 'use_password'=>(md5($password)), 'use_name'=>$name, 'use_lastname'=>$lastname);
			$uid = $this->registroDataTable->insert($data);
			$userxstatus = new User_Model_DbTable_UserXStatus();
			$userxstatus->insert(Array('use_id'=>$uid, 'use_sta_id'=>1));
			return $uid;
    }
	
	public function activateUser($uid, $name, $email){
		$data = Array('use_sta_id'=>2);
		$where = "use_id = {$uid} AND use_name = '{$name}' AND use_email = '{$email}'";
		if ( $this->registroDataTable->update($data, $where) ){
			$userxstatus = new User_Model_DbTable_UserXStatus();
			$userxstatus->insert(Array('use_id'=>$uid, 'use_sta_id'=>2));
			return true;
		}
		return false;
	}
	
	public function isEmail($email){
		$where = "use_email = '{$email}'";
		$items = $this->registroDataTable->getItems($where);
		if (count($items) > 0){
			return true;
		}else {
			return false;
		}
	}
    
}

