<?php
/**
 * class AccountController
 * 
 */
class User_AccountController extends Zend_Controller_Action
{
	
	/**
	 * @var Zend_Config_Ini $textGlobal
	 * @var Zend_Config_Ini $textModule
	 */
	private $textGlobal;
	private $textModule;
	
    public function init(){
    	$this->textGlobal = App_Util_Language::getTextLanguage();
        $this->textModule = App_Util_Language::getTextLanguage($this->getRequest()->getModuleName()); 
        $this->view->placeholder("title")->set($this->textModule->index->title);
    }
    
    /**
     * Action index
     * 
     * Action de inicio por defecto
     */
    public function indexAction ()
    {
		$this->_helper->layout->setLayout('login');
        $this->view->title = "Account | ".$this->textModule->index->label;
        $this->view->headTitle($this->view->title);
    }
	
	/**
     * Action signUp
     * 
     */
    public function signUpAction ()
    {
        $this->view->title = "Account | ".$this->textModule->index->label;
        $this->view->headTitle($this->view->title);
    }
	
	/**
     * Action signUp
     * 
     */
    public function saveUserAction ()
    {
    	$this->_helper->layout()->disableLayout ();
    	$this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$user = new User_Model_User();
		$new_user = $user->saveElement(trim($params['sign_up-email']), trim($params['sign_up-fname']), trim($params['sign_up-lname']), trim($params['sign_up-password']));
    	Zend_Debug::dump($new_user);
	}
	
	/**
     * Action confirm-user
     * 
     */
    public function confirmUserAction ()
    {
    	$this->view->title = "Account | ".$this->textModule->index->label;
        $this->view->headTitle($this->view->title);
		$this->_helper->layout()->disableLayout ();
    	$this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$user = new User_Model_User();
		if ( $user->activateUser(trim($params['i']), trim($params['n']), trim($params['e'])) ){
			echo 'success';
		}else {
			echo 'unsuccess';
		}
	}
	
	
	/**
     * Action is-user
     * 
     */
    public function isUserAction ()
    {
    	$this->_helper->layout()->disableLayout ();
    	$this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$user = new User_Model_User();
		if ( $user->isEmail(trim($params['new_email'])) ){
			echo 1;
		}else {
			echo 0;
		}
	}
	
	
	/**
     * Action signUp
     * 
     */
    public function testAction ()
    {
    	$this->_helper->layout()->disableLayout ();
    	$this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$user = new User_Model_User();
		$user->login($params['email'], $params['password']);
	}
	

    
}
?>
