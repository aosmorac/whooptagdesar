<?php

class Messages_Model_DbTable_NewsxEmail extends Zend_Db_Table_Abstract {

    protected $_name = "newsxemail";
    protected $_primary = 'new_id';

    public function __construct() {
        $this->_setAdapter('NEWS');
    }
	public function addNewxMail($new_id,$ema_id){
	$data=Array('new_id'=>$new_id,'email_id'=>$ema_id);
	$this->insert($data);
	}
	public function countMail($new_id){
	$where=("new_id={$new_id}"); 
	$select=$this->select()
		->from('newsxemail',array('con'=>'count(*)'))
		->where($where);
	$cont = $this->fetchRow($select);
	$contmail = $cont->toArray();
	//Zend_Debug::dump($select);
    return $contmail;
	}
}