<?php

class Messages_Model_DbTable_News extends Zend_Db_Table_Abstract {

    protected $_name = "news";
    protected $_primary = 'new_id';

    public function __construct() {
        $this->_setAdapter('NEWS');
    }
	public function  getNew($new_id){
	$select=$this->select()
			->where('new_id=?',$new_id);
	$row = $this->fetchAll($select);
	return $row;
	}
	
}