<?php

class Messages_Model_DbTable_Emails extends Zend_Db_Table_Abstract {

    protected $_name = "emails";
    protected $_primary = 'ema_id';

    public function __construct() {
        $this->_setAdapter('NEWS');
    }
	public function listMail($mailxiter,$new_id){
		$where="NXE.email_id IS NULL AND E.ema_status=-1";
		$select = $this->select()
                  ->from(array("E"=>$this->_name))
                  ->setIntegrityCheck(false)
                  ->joinLeft(array('NXE' => 'newsxemail'),"E.ema_id=NXE.email_id AND NXE.new_id={$new_id} "
                        , array())
                ->where($where)
				->limit($mailxiter);
		//Zend_Debug::dump($select.''); die;
		$row = $this->fetchAll($select);
        $listMail = $row->toArray();
        return $listMail;
	}
	public function contMail(){
		$select=$this->select()
			->from('emails',array('n'=>'count(*)'))
			->where("ema_status=-1");
		$cont = $this->fetchRow($select);
		//Zend_Debug::dump($select.''); die;
		$contmail = $cont->toArray();
		return $contmail;
	}
	
}