<?php
/**
 * class SiteController
 * 
 * Clase de inicio por defecto del sistema
 */
class SearchController extends Zend_Controller_Action
{
	
	/**
	 * @var Zend_Config_Ini $textGlobal
	 * @var Zend_Config_Ini $textModule
	 */
	private $textGlobal;
	private $textModule;
	
    public function init(){
    	$this->textGlobal = App_Util_Language::getTextLanguage();
        $this->textModule = App_Util_Language::getTextLanguage($this->getRequest()->getModuleName()); 
        $this->view->placeholder("title")->set($this->textModule->index->title);
		$this->_helper->layout->setLayout('general');
    }
    
    /**
     * Action index
     * 
     * Action de inicio por defecto
     */
    public function indexAction ()
    {
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        $this->view->title = "Home | ".$this->textModule->index->label;
        $this->view->headTitle($this->view->title);  
    }
    
    public function allAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
		$tag = trim($this->getParam('tag'));
		if ($keysearch = App_Util_Keysearch::getKeySearch(str_replace(' ', '', $tag))) {
			$this->view->headTitle($keysearch['title'].' - Whooptag.com'); 
			$this->view->headMeta($keysearch['description'], 'description');
		}else{
			$this->view->headTitle($tag.' - Whooptag.com'); 
			$this->view->headMeta($this->textModule->index->description, 'description');
		}
		$this->view->tag = $tag;  
    }
    
	
	
	
	/**
	 * Genera resultado para busqueda en instagram
	 * 
	 */
    public function instagramAction(){
    	
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        setlocale(LC_COLLATE, 'en_US.utf8');
		
		$tag = trim($this->getParam('tag'));
		$s = $this->getParam('s');
		$u = $this->getParam('u'); 
		
		// Titulo y descripcion de la pagina -----------------
		if ($keysearch = App_Util_Keysearch::getKeySearch(str_replace(' ', '', $tag))) {
			$this->view->headTitle($keysearch['title'].' - Whooptag.com'); 
			$this->view->headMeta($keysearch['description'], 'description');
			//Zend_Debug::dump($keysearch); die;
		}else{
			$this->view->headTitle($tag.' - Whooptag.com'); 
			$this->view->headMeta($this->textModule->index->description, 'description');
		}
		// ------------------------------------------
		
		// Divide la busqueda en todas sus palabras---
		$words = explode(' ', trim($tag));
		$tags = array();
		$pref = '';
		$tags[0] = '';
		if ( count($words) > 1 ){
			foreach ($words AS $w) {
				if ( $tags[0] != '' ) {
					$tags[0] .= ucwords(App_Util_String::remove_accents($w));
				}else {
					$tags[0] .= App_Util_String::remove_accents($w);
				}
				if ( strlen($w) > 3 ){
					if ( $pref != '' ){
						$tags[] = $pref.ucwords(App_Util_String::remove_accents($w));
						$pref = '';
					}
					$tags[] = App_Util_String::remove_accents($w);
				}else{
					if ( $pref != '' ){
						$pref = $pref.ucwords(App_Util_String::remove_accents($w));
					}else{
						$pref = $pref.App_Util_String::remove_accents($w);
					}
				}
			}
		}else{
			$tags[0] = App_Util_String::remove_accents(trim($tag));
		}
		$tags = array_unique($tags);
		if ( !isset($s) ){
			$s = $tags[0];
		}
		// -----------------------------------------------
		
		if ( !isset($u) ){
			$u = 0;	// Si no es un usuario asigna 0
		}
		$this->view->s = $s;  
		$this->view->u = $u; 
		$this->view->tag = $tag;  
		$this->view->tags = $tags; 
		
		
		// Si es url directo a un elemento	------------
		$params = $this->getAllParams();
		if (isset($params['pid'])){
			$this->view->isElement = true;  
			$this->view->pid = $params['pid'];  
			$media = new Site_Model_Media();
			$element = $media->getElement($params['pid'], 'instagram');
			$this->view->linkedElement = json_encode($element);  
		}else{
			$this->view->isElement = false;  
		}
		//----------------------------------------------
		
    }
    
	
	
	
	
	
	
	
	
    public function twitterAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        setlocale(LC_COLLATE, 'en_US.utf8');
		
		$tag = trim($this->getParam('tag'));
		$s = $this->getParam('s');
		$u = $this->getParam('u'); 
		
		// Titulo y descripcion de la pagina -----------------
		if ($keysearch = App_Util_Keysearch::getKeySearch(str_replace(' ', '', $tag))) {
			$this->view->headTitle($keysearch['title'].' - Whooptag.com'); 
			$this->view->headMeta($keysearch['description'], 'description');
		}else{
			$this->view->headTitle($tag.' - Whooptag.com'); 
			$this->view->headMeta($this->textModule->index->description, 'description');
		}
		// ------------------------------------------
		
		// Divide la busqueda en todas sus palabras---
		$words = explode(' ', trim($tag));
		$tags = array();
		$pref = '';
		$tags[0] = '';
		if ( count($words) > 1 ){
			foreach ($words AS $w) {
				if ( $tags[0] != '' ) {
					$tags[0] .= ucwords(App_Util_String::remove_accents($w));
				}else {
					$tags[0] .= App_Util_String::remove_accents($w);
				}
				if ( strlen($w) > 3 ){
					if ( $pref != '' ){
						$tags[] = $pref.ucwords(App_Util_String::remove_accents($w));
						$pref = '';
					}
					$tags[] = App_Util_String::remove_accents($w);
				}else{
					if ( $pref != '' ){
						$pref = $pref.ucwords(App_Util_String::remove_accents($w));
					}else{
						$pref = $pref.App_Util_String::remove_accents($w);
					}
				}
			}
		}else{
			$tags[0] = App_Util_String::remove_accents(trim($tag));
		}
		$tags = array_unique($tags);
		if ( !isset($s) ){
			$s = $tags[0];
		}
		// -----------------------------------------------
		
		// Obtiene usuarios relacionados ----------------
		$users = App_Util_Twitter::getUsers($tag);
		$this->view->users = $users; 
		//-----------------------------------------------
		
		if ( !isset($u) ){
			$u = 0;	// Si no es un usuario asigna 0
		}
		$this->view->s = $s;  
		$this->view->u = $u; 
		$this->view->tag = $tag;  
		$this->view->tags = $tags; 
		
		// Si es url directo a un elemento	------------
		$params = $this->getAllParams();
		if (isset($params['pid'])){
			$this->view->isElement = true;  
			$this->view->pid = $params['pid'];  
			$media = new Site_Model_Media();
			$element = $media->getElement($params['pid'], 'twitter');
			$this->view->linkedElement = json_encode($element);  
		}else{
			$this->view->isElement = false;  
		}
		//----------------------------------------------
    }
    /*
     * Retorna veinte twitts por pagina
    */
    public function getTwitterUsersAction(){
    	header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
    	$this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	$params = $this->getAllParams();
    	$tag = trim($params['tag']);
    	// Obtiene usuarios relacionados ----------------
		$users = App_Util_Twitter::getUsers($tag);
    	echo json_encode($users);
		//-----------------------------------------------
    }
	/*
	 * Retorna veinte twitts por pagina
	 */
	public function getTwittersAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$tag = trim($params['tag']);
		if ( isset($params['max_id']) ){
			$max_id = $params['max_id'];
		}else {
			$max_id = 0;
		}
		if ($max_id < 0){
			$max_id = 0;
		}
		$twitter = new App_Util_Twitter();
		$twits = $twitter->getTweets($tag, 25, $max_id);
		echo json_encode($twits);
	}
	
	
	/*
	 * Retorna veinte twitts por pagina
	 */
	public function getTwittersByUserAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$uid = trim($params['uid']);
		if ( isset($params['max_id']) ){
			$max_id = $params['max_id'];
		}else {
			$max_id = 0;
		}
		if ($max_id < 0){
			$max_id = 0;
		}
		$twitter = new App_Util_Twitter();
		$twits = $twitter->getTweetsByUser($uid, 25, $max_id);
		echo json_encode($twits);
	}
	
	
    
    public function youtubeAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
		$tag = trim($this->getParam('tag'));
		$params = $this->getAllParams();
		if ( isset($params['o']) ){
			$o = $params['o'];
		}else {
			$o = 'date';
		}
		if ($keysearch = App_Util_Keysearch::getKeySearch(str_replace(' ', '', $tag))) {
			$this->view->headTitle($keysearch['title'].' - Whooptag.com'); 
			$this->view->headMeta($keysearch['description'], 'description');
		}else{
			$this->view->headTitle($tag.' - Whooptag.com'); 
			$this->view->headMeta($this->textModule->index->description, 'description');
		}
		$this->view->tag = $tag;
		$this->view->o = $o;  
		
		// Si es url directo a un elemento	------------
		$params = $this->getAllParams();
		if (isset($params['pid'])){
			$this->view->isElement = true;  
			$this->view->pid = $params['pid'];  
			$media = new Site_Model_Media();
			$element = $media->getElement($params['pid'], 'youtube');
			$this->view->linkedElement = json_encode($element);  
		}else{
			$this->view->isElement = false;  
		}
		//----------------------------------------------
    }
	public function getYoutubeAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$tag = trim($params['tag']);
		$order = 'date';
		if ( isset($params['order']) ){
			$order = $params['order'];
		}else {
			$order = 'date';
		}
		if ( isset($params['page']) ){
			$page = $params['page'];
		}else {
			$page = '';
		}
		$youtube = new App_Util_Youtube();
		$videos = $youtube->searchVideos($tag, 20, $order, $page);
		echo json_encode($videos);
	}
	
	
	
	public function mapAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
		$tag = trim($this->getParam('tag'));
		$this->view->headTitle($tag.' - Whooptag.com'); 
		$this->view->headMeta($this->textModule->index->description, 'description');
		$this->view->tag = $tag;  
	}
	
	
	public function saveMediaAction(){
        header("Access-Control-Allow-Origin: *");   //  Ajax desde cualquier llamado
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
		$params = $this->getAllParams();
		$element = new Site_Model_Media();
		echo $element->saveElement($params['network_id'], $params['element_data'], $params['network']);
	}
    
	
	
	

    
}
?>
