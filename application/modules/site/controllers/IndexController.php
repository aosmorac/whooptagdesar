<?php
/**
 * class IndexController
 * 
 * Clase de inicio por defecto del sistema
 */
class IndexController extends Zend_Controller_Action
{
	
	/**
	 * @var Zend_Config_Ini $textGlobal
	 * @var Zend_Config_Ini $textModule
	 */
	private $textGlobal;
	private $textModule;
	
    public function init(){
    	$this->textGlobal = App_Util_Language::getTextLanguage();
        $this->textModule = App_Util_Language::getTextLanguage($this->getRequest()->getModuleName()); 
        $this->view->placeholder("title")->set($this->textModule->index->title);
    }
    
    /**
     * Action index
     * 
     * Action de inicio por defecto
     */
    public function indexAction ()
    {
        $this->view->title = "Home | ".$this->textModule->index->label;
		$this->view->description = $this->textModule->index->description;
        $this->view->headTitle($this->view->title);
		$this->view->headMeta($this->view->description, 'description');
    }
	
	
	public function instagramAction ()
	{
		
	}
	

    
}
?>
