<?php

class Site_Model_Media {
	
    private $registroDataTable;
	
    public function __construct() {
        $this->registroDataTable = new Site_Model_DbTable_Media();
    }
	
    public function saveElement($source_id, $element_data, $network='instagram'){
    	$rows = $this->registroDataTable->fetchAll(
		    $this->registroDataTable->select()
		        ->where("med_source_id = '{$source_id}' AND net_alias = '{$network}'")
		        ->limit(10, 0)
	    );
		if ( count($rows) > 0 ){
			return 0;
		}else {
    		$data = Array('med_data'=>$element_data, 'net_alias'=>$network, 'med_source_id'=>$source_id);
			return $this->registroDataTable->insert($data);
		}
    }
	
	public function getElement($source_id, $network = 'instagram'){
		$element = $this->registroDataTable->getElement($source_id, $network);
		if ( isset($element['med_data']) )
			return $element['med_data'];
		else 
			return '';
	}
    
}

