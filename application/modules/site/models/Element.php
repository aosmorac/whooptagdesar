<?php

class Site_Model_Element {
	
    private $registroDataTable;
	
    public function __construct() {
        $this->registroDataTable = new Site_Model_DbTable_Element();
    }
	
    public function saveElement($network_id, $element_data, $network='instagram'){
    	$rows = $this->registroDataTable->fetchAll(
		    $this->registroDataTable->select()
		        ->where("elem_network_id = {$network_id} AND elem_network = {$network}")
		        ->limit(10, 0)
	    );
		if ( count($rows) > 0 ){
			return 0;
		}else {
    		$data = Array('elem_data'=>$element_data, 'elem_network'=>$network, 'elem_network_id'=>$network_id);
			return $this->registroDataTable->insert($data);
		}
    }
    
}

