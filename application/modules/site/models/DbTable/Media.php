<?php

class Site_Model_DbTable_Media extends Zend_Db_Table_Abstract {

    protected $_name = "media";

    public function __construct() {
        $this->_setAdapter('SITE');
    }
    
	
	public function getElement($source_id, $network = 'instagram'){
		$row = $this->fetchRow(
            $this->select()
                ->where("net_alias='{$network}' AND med_source_id='{$source_id}'")
                ->limit(1)
        );
		if (isset($row))
        	$row = $row->toArray();
		else 
			$row = array();
        return $row;
	}


}

