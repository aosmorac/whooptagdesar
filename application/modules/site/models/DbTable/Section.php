<?php

class Sites_Model_DbTable_Section extends Zend_Db_Table_Abstract {

    protected $_name = "sec_id";

    public function __construct() {
        $this->_setAdapter('SITE');
    }
    
    public function getSectionByAlias($alias){
        $select = $this->select() 
            ->from(array('SEC' => 'section')
                         , array(
                             'sec_id', 'sec_name', 'sec_alias', 
                             'sec_description', 'sec_date', 
                             'sec_lat', 'sec_lng', 
                             'instagram'=>new Zend_Db_Expr("(SELECT tag_name FROM tags WHERE net_name = 'instagram' AND sec_id = SEC.sec_id AND tag_active = 1 LIMIT 1)"), 
                             'twitter'=>new Zend_Db_Expr("(SELECT tag_name FROM tags WHERE net_name = 'twitter' AND sec_id = SEC.sec_id AND tag_active = 1 LIMIT 1)"), 
                             'youtube'=>new Zend_Db_Expr("((SELECT tag_name FROM tags WHERE net_name = 'youtube' AND sec_id = SEC.sec_id AND tag_active = 1 LIMIT 1)") 
                            )
                    )
            ->setIntegrityCheck(false)
            ->where("SEC.sec_active = 1");
        return $this->fetchRow($select);
    }


}

